Dear CRAN Team,
this is a resubmission of package 'cleanr'. 

In response to Victoria Wimmer's mail, I have omitted examples in documentation
of unexported functions as requested by CRAN.

As to:
> Please proof-read your description text.
> Currently it reads: "...Note: This is not a static code analyzer like pylint
> or the like."
> -> does not seem like a full sentence.
`or the like` is a common phrase translating to the German "oder dergleichen".
So to me the above sentence seems like a full one and would translate to 
`Dies [ie. package cleanr] ist kein statischer Code-Analyser wie pylint oder
dergleichen.` 

As to the comments on not writing anywhere else than to tempdir() and not
modifiying .GlobalEnv: package cleanr doesn't do any of them. I guess this was
more a common disclaimer than based on any evidence related to package cleaner.

Please upload to CRAN.
Best, Andreas Dominik

# Package cleanr 1.4.0

Reporting is done by packager version 1.15.0


## Test environments
- R Under development (unstable) (2023-06-03 r84490)
   Platform: x86_64-pc-linux-gnu
   Running under: Devuan GNU/Linux 4 (chimaera)
   0 errors | 0 warnings | 1 note 
- win-builder (devel)  
  Date: Thu, 15 Jun 2023 22:49:53 +0200
  Your package cleanr_1.4.0.tar.gz has been built (if working) and checked for Windows.
  Please check the log files and (if working) the binary package at:
  https://win-builder.r-project.org/bh02m6aA15Z8
  The files will be removed after roughly 72 hours.
  Installation time in seconds: 2
  Check time in seconds: 48
  Status: 1 WARNING, 1 NOTE
  R Under development (unstable) (2023-06-14 r84541 ucrt)
   

## Local test results
- RUnit:
    cleanr_unit_test - 21 test functions, 0 errors, 0 failures in 51 checks.
- testthat:
    [ FAIL 0 | WARN 0 | SKIP 0 | PASS 1 ]
- Coverage by covr:
    cleanr Coverage: 98.99%

## Local meta results
- Cyclocomp:
     no issues.
- lintr:
    found 58 lints in 860 lines of code (a ratio of 0.0674).
- cleanr:
    found 2 dreadful things about your code.
- codetools::checkUsagePackage:
    found 1 issues.
- devtools::spell_check:
    found 14 unkown words.
