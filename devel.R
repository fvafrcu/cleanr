if (FALSE) {
    packager::set_package_info(path = ".", title = "YOUR TITLE HERE",
                     description = paste("Multiline",
                                         "description here."),
                     details = paste("More",
                                     "details here."),
                     force = packager:::is_force()
                     )
}

pkgload::load_all(".")
fritools::find_missing_see_also(".")
fritools::find_missing_family(".")
fritools::delete_trailing_whitespace(path = "R", pattern = "\\.[rR]$")
fritools::delete_trailing_blank_lines(path = "R", pattern = "\\.[rR]$")
fritools::delete_trailing_whitespace(path = "inst/runit_tests", pattern = "\\.[rR]$")
